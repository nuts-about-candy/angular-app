export class ProductSize {
    id : number;
    size : string;
    label : string;
    prize : number;
    weight : number;
    prodClassId : number;
}