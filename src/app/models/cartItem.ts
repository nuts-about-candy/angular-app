import { Product } from './product';
import { ProductSize } from './productSize';

export class CartItem {
    id : number;
    userId : number;
    productId : number;
    size : string;
    quantity : number;

    productDetails : Product;
    productSize : ProductSize;
    
    totalWeight : number;
    subTotal : number;
}