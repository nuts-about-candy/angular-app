export class User{
    id : number;
    userName : string;
    firstName : string;
    lastName : string;
    access : string;
    accessId : number;
}