import { CartItem } from './cartItem';
import { User } from './user';

export class CheckoutDetails {
    id : number;
    user : User;
    cartItems : CartItem[];
    deliveryFee : number;
    subTotal : number;
    total : number;
    totalWeight : number;
    discount : number;
    discountNote : string;
    deliveryNote : string;
    shelfLifeNuts : string;
    shelfLifeCandies : string;
    itemCount : number;
}