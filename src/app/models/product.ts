import { ProductSize } from './productSize';

export class Product {
    id: number;
    name: string;
    description : string;
    stock : number;
    category : string;
    categoryId : number;
    classification : string;
    classificationId : number;
    productSizes : ProductSize;
    img : string;
}