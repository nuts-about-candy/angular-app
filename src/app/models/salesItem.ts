export class SalesItem {
    category : string;
    productName : string;
    classification : string;
    quantity : number;
    price : number;
    weight : number;
}