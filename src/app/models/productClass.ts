export class ProductClass {
    id : number;
    name : string;
    small : number;
    medium : number;
    large : number;
}