import { SalesItem } from './salesItem';

export class SalesReport {
    grossSales : number;
    totalDiscount : number;
    netSales : number;
    salesItems : SalesItem[];
    totalQuantity : number;
    totalPrice : number;
    totalWeight : number;
}