import { Injectable } from '@angular/core';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  private endpointInventory = "http://localhost/api/inventory";
  private endpointCategory = "http://localhost/api/inventory/category";
  private endpointProductClass = "http://localhost/api/inventory/product-class";

  // header = {
  //   headers: new HttpHeaders()
  //     .set('Authorization',  localStorage.getItem("Token"))
  // }
  constructor(private http: HttpClient) { }

  getProductClass() {
    return this.http.get<HttpResponse<any>>
        (`${this.endpointProductClass}`);
  }

  getCategories() {
    return this.http.get<HttpResponse<any>>
        (`${this.endpointCategory}`);
  }

  getProductByProductId(productId) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpointInventory}` + "/" + productId);
  }
  getProductsByCategoryId(categoryId) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpointCategory}` + "/" + categoryId);
  }

  addCategory(data) : Observable<any>{
    return this.http.post<HttpResponse<any>>
        (`${this.endpointCategory}`, data);
  }

  addProduct(data) : Observable<any>{
    return this.http.post<HttpResponse<any>>
        (`${this.endpointInventory}`, data);
  }
  editProduct(data) : Observable<any>{
    return this.http.put<HttpResponse<any>>
        (`${this.endpointInventory}`, data);
  }

  deleteCategory(id) {
    return this.http.delete(this.endpointCategory+ "/" + id);
  }
  deleteProduct(id) {
    return this.http.delete(this.endpointInventory+ "/" + id);
  }

  resetStocks() : Observable<any>{
    return this.http.post<HttpResponse<any>>
        (`${this.endpointInventory}` + "/reset", {});
  }

  addStocks() : Observable<any>{
    return this.http.post<HttpResponse<any>>
        (`${this.endpointInventory}` + "/add", {});
  }
}
