import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  private endpointProducts = "http://localhost/api/inventory/products";
  // header = {
  //   headers: new HttpHeaders()
  //     .set('Authorization',  localStorage.getItem("Token"))
  // }
  
  getProductForSaleByProductId(productId: number){
    return this.http.get<HttpResponse<any>>
        (`${this.endpointProducts}` + "/" + productId);
  }

  getProductsForSaleByCategoryId(categoryId : number) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpointProducts}` + "/category/" + categoryId);
  }
}
