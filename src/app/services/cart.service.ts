import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { CartItem } from '../models/cartItem';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private endpoint = "http://localhost/api/cart";
  // header = {
  //   headers: new HttpHeaders()
  //     .set('Authorization',  localStorage.getItem("Token"))
  // }

  constructor(private http: HttpClient) { }

  getCartItems(userId : number) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpoint}` + "/" + userId);
  }

  addItem(data : CartItem) {
    return this.http.post<HttpResponse<any>>
        (`${this.endpoint}`, data);
  }

  removeItem(cartId : number) {
    return this.http.delete(this.endpoint+ "/" + cartId);
  }
}
