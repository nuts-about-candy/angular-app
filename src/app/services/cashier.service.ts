import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CashierService {

  endpoint = "http://localhost/api/cashier";
  constructor(private http: HttpClient) { }

  checkout(data) {
    return this.http.post<HttpResponse<any>>
        (`${this.endpoint}` + "/checkout", data);
  }

  getOrderHistory(userId) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpoint}` + "/order-history/" + userId);
  }

  getSalesReport(yearMonth : string, param : string) {
    return this.http.get<HttpResponse<any>>
        (`${this.endpoint}` + "/sales-report/" + yearMonth + "?" +param);
  }

  getReportMonths() {
    return this.http.get<HttpResponse<any>>
        (`${this.endpoint}` + "/report-months");
  }
}
