import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate{

  private loginEndpoint = "http://localhost/api/user/login";
  private userEndpoint = "http://localhost/api/user";
  

  constructor(private http: HttpClient,
    private router: Router) { }

  login(data) : Observable<any>{
    return this.http.post<HttpResponse<any>>
        (`${this.loginEndpoint}`, data, { observe: 'response' });
  }

  getUserById(userId) {

    return this.http.get<HttpResponse<any>>
        (`${this.userEndpoint}` + "/" + userId);
  }

  canActivate() {
    let emp = JSON.parse(localStorage.getItem("userDetails"));
    if (emp == null || emp == undefined) {
        this.router.navigate(['/login']);
        return false;
    }
    return true;
  }

}
