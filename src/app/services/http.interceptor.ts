import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token: string = localStorage.getItem('Token');

    if(request.url.search("/login") === -1) {
      if (token) 
        request = request.clone(
          { headers: request.headers.set('Authorization', token) }
        );

      return next.handle(request).pipe(
        map((event : HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            console.log('event--->>>', event);
          }
          return event;
        })
        // ,catchError((error : HttpErrorResponse) => {
        //   console.log(error.status);
        //   let errorMessage = '';
        //   if (error.error instanceof ErrorEvent) {
        //     // client-side error
        //     errorMessage = `Error: ${error.error.message}`;
        //   } else {
        //     // server-side error
        //     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        //   }
        //   return throwError(errorMessage);
        // })
      );
    }
    else
      return next.handle(request);
  }
}
