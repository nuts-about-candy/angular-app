import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {MyHttpInterceptor} from './services/http.interceptor';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { ProductsComponent } from './components/products/products.component';
import { ReportsComponent } from './components/reports/reports.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from './services/user.service';
import { CartComponent } from './components/cart/cart.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';
import { ProductInfoComponent } from './components/product-info/product-info.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {title : "User Login"}
  },
  {
    path: 'inventory',
    component: InventoryComponent,
    canActivate : [UserService],
    data: {title : "Admin Inventory Page"}
  },
  {
    path: 'reports',
    component: ReportsComponent,
    canActivate : [UserService],
    data: {title : "Reports"}
  },
  {
    path: 'nuts-about-candies',
    component: ProductsComponent,
    canActivate : [UserService],
    data: {title : "Products For Sale"}
  },
  {
    path: 'nuts-about-candies/:id',
    component: ProductInfoComponent,
    canActivate : [UserService],
    data: {title : "Product Info"}
  },
  {
    path: 'my-cart',
    component: CartComponent,
    canActivate : [UserService],
    data: {title : "Items for Checkout"}
  },
  {
    path: 'order-history',
    component: OrderHistoryComponent,
    canActivate : [UserService],
    data: {title : "Items for Checkout"}
  },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**',
    component: PagenotfoundComponent,
  }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
    InventoryComponent,
    ProductsComponent,
    ReportsComponent,
    CartComponent,
    OrderHistoryComponent,
    ProductInfoComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass : MyHttpInterceptor,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
