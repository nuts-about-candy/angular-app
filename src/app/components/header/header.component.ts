import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user = {} as User;
  public isMenuCollapsed = true;
  @Input() cartItemCount : number;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("userDetails"));
  }

  logout() {
    localStorage.removeItem("Token");
    localStorage.removeItem("userDetails");
    this.router.navigate(['/login']);
  }
}
