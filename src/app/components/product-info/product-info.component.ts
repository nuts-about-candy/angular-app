import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { User } from 'src/app/models/user';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ProductSize } from 'src/app/models/productSize';
import { CartItem } from 'src/app/models/cartItem';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private productService : ProductService, 
    private cartService : CartService,
    private title : Title) { }

  productId;
  product = {} as Product;
  cartItemCount : number = 0;
  user = {} as User;
  formGroupProduct : FormGroup;

  ngOnInit(): void {
    this.title.setTitle("Product Details | Nuts About Candy")
    this.productId = this.route.snapshot.paramMap.get('id');
    this.user = JSON.parse(localStorage.getItem("userDetails"));
    this.getCartItemsCount();
    this.getProductByProductId(this.productId);
    this.initFormGroup();
  }

  initFormGroup() {
    this.formGroupProduct = this.formBuilder.group({
      size : new FormControl('', [Validators.required]),
      quantity : new FormControl(1, [Validators.required])
    })
  }
  getCartItemsCount() {
    this.cartService.getCartItems(this.user.id)
      .subscribe((res : any) => {
        this.cartItemCount = res.itemCount;
      },
      error => console.log("Error in getting user cart items."));
  }
  getProductByProductId(id : number) {
    this.productService.getProductForSaleByProductId(id)
      .subscribe((res : any) => {
        this.product = res;
        this.product.img = "assets/img/nac-prod/nac"+ 
          Math.round((Math.random() * 12) + 1) + ".jpg";
      }, error => console.log(error));
  }

  addToCart(productId : number, productSizes : any) {

    let cartItem = {} as CartItem;
    cartItem = this.formGroupProduct.value;
    cartItem.userId = this.user.id;
    
    if(cartItem.size === "")
      alert("Error! Please select Product size.");
    else 
      if(cartItem.quantity === null || cartItem.quantity <= 0) 
        alert("Error! Invalid value for quantity.");
      else
        this.productService.getProductForSaleByProductId(productId)
          .subscribe((res: any) => {
            let product : Product = res;
            cartItem.productDetails = product;
            cartItem.productId = product.id;

            let productSize = productSizes.filter(size => {
              return size.size == cartItem.size;
            })[0];
            
            cartItem.productSize = productSize;
            console.log(cartItem);

            if((cartItem.quantity * productSize.weight) > product.stock)
              alert("Error! Selected quantity exceeds current stock.");
            else
              this.cartService.addItem(cartItem)
                .subscribe((res : any) => {
                  alert("Success! Item added to your cart.");
                  this.getCartItemsCount();
                  this.getProductByProductId(productId);
                }, error => console.log("Error adding item to cart."));

          }, error => console.log("Error getting details of Product for sale."));
  }
}
