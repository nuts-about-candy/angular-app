import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;
  invalidCredentialError = false;
  connectionError = false;

  constructor(private userService : UserService,
    private router: Router,
    private title : Title) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.title.setTitle("Login | Nuts About Candy")
    this.formGroup = new FormGroup({
      username : new FormControl('', [Validators.required]),
      password : new FormControl('', [Validators.required])
    })
  }

  getEmployeeDetails(decodedToken) {
    let emp = decodedToken.sub;
    let userId = emp.split(".")[1];

    this.userService.getUserById(userId)
      .subscribe((res : any) => {
        
        localStorage.setItem("userDetails", JSON.stringify(res));
        let userDetails = JSON.parse(localStorage.getItem("userDetails"));

        if(userDetails.access === "ADMIN") {
          this.router.navigate(['/inventory']);
        } 
        else {
          this.router.navigate(['/nuts-about-candies']);
        }
        
      },
      error => {
        console.log(error);
        this.connectionError = true;
      })
  }
  
  login() {
    if(this.formGroup.valid) {
      this.userService.login(this.formGroup.value)
        .subscribe((result : any) => {
          localStorage.setItem("Token", result.body.Authorization);
          let token = result.body.Authorization.replace("Bearer ", "");
          let decodedToken = jwt_decode(token);
          this.getEmployeeDetails(decodedToken);
        },
        error => {
          this.invalidCredentialError = true;
        }
      )

    }
  } // end : login() {
}
