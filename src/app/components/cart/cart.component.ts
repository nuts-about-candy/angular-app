import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CartService } from 'src/app/services/cart.service';
import { User } from 'src/app/models/user';
import { CartItem } from 'src/app/models/cartItem';
import { CheckoutDetails } from 'src/app/models/checkoutDetails';
import { CashierService } from 'src/app/services/cashier.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItemCount : number = 0;
  cartItems : CartItem[];
  checkoutDetails = {} as CheckoutDetails;
  user = {} as User;
  checkoutSuccess = false;
  checkoutError = "";
  
  constructor(private title : Title,
    private cartService : CartService,
    private cashierService : CashierService) { }
  
  ngOnInit(): void {;
    this.title.setTitle("Items for Checkout | Nuts About Candy");
    this.user = JSON.parse(localStorage.getItem("userDetails"));
    this.getCartItems();
    this.checkoutSuccess = false;
  }

  getCartItems() {
    this.cartService.getCartItems(this.user.id)
      .subscribe((res : any) => {
        this.checkoutDetails = res;
        // console.log(this.checkoutDetails);
        this.cartItems = this.checkoutDetails.cartItems;
        this.cartItemCount = this.cartItems.length;
      },
      error => {
        console.log("Error in getting user cart items.");
      })
  }
  
  removeToCart(cartId : number, name) {
    let confirm = window.confirm("Are you sure you want to remove '"+ name +"' to your cart?");
    if(confirm)
      this.cartService.removeItem(cartId)
        .subscribe((res : any) => {
          this.getCartItems();
        },
        error => {
          console.log("Error in removing item in cart");
        })
  }

  checkout(data : CheckoutDetails) {
    let confirm = window.confirm("The Total Amount Payable is '"+ data.total +"'."
      + "\nAre you sure you want to proceed Checkout?");
    if(confirm) {
      data.user = this.user;
      this.cashierService.checkout(data)
        .subscribe(res => {
          this.getCartItems();
          this.checkoutSuccess = true;
        }, error =>  this.checkoutError = error.error.message);
    }
    
  }
}
