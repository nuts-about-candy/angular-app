import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { CartService } from 'src/app/services/cart.service';
import { CashierService } from 'src/app/services/cashier.service';
import { CheckoutDetails } from 'src/app/models/checkoutDetails';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  cartItemCount : number = 0;
  orderHistory = [] as CheckoutDetails[];
  user = {} as User;
  constructor(private cartService : CartService,
    private cashierService : CashierService,
    private title : Title) { }

  ngOnInit(): void {
    this.title.setTitle("Order History | Nuts About Candy")
    this.user = JSON.parse(localStorage.getItem("userDetails"));
    this.getCartItemsCount();
    this.getOrderHistory();
  }

  getCartItemsCount() {
    this.cartService.getCartItems(this.user.id)
      .subscribe((res : any) => {
        this.cartItemCount = res.itemCount;
      },
      error => console.log("Error in getting user cart items."));
  }

  getOrderHistory() {
    this.cashierService.getOrderHistory(this.user.id)
      .subscribe((res : any) => {
        this.orderHistory = res;
      }, error => console.log(error));
  }
}
