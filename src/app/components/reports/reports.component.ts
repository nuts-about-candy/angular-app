import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CashierService } from 'src/app/services/cashier.service';
import { ReportMonth } from 'src/app/models/reportMonth';
import { SalesReport } from 'src/app/models/salesReport';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor(private title : Title,
    private cashierService : CashierService) { }

  yearMonth;
  reportMonth = [] as ReportMonth[];
  salesReport = {} as SalesReport;
  isCategoryAsc = false;
  isProductNameAsc = true;
  isQuantityAsc = true;
  isWeightAsc = true;
  isPriceAsc = true;

  ngOnInit(): void {
    this.title.setTitle("Reports | Nuts About Candy");
      this.getReportMonths();
  }

  getReportMonths() {
    this.cashierService.getReportMonths()
      .subscribe((res : any) => {
        this.reportMonth = res;
        this.yearMonth = this.reportMonth[0].value;
        this.sortCategory();
      }, error => console.log(error));
  }

  getSalesReport(yearMonth : string, param : string){
    this.cashierService.getSalesReport(yearMonth, param)
      .subscribe((res : any) => {
        this.salesReport = res;
      }, error => console.log(error));
  }

  sortCategory() {
    this.isCategoryAsc = !this.isCategoryAsc;
    let param = this.isCategoryAsc ? "category=asc" : "category=desc";
    this.getSalesReport(this.yearMonth, param);
  }

  sortProductName() {
    this.isProductNameAsc = !this.isProductNameAsc;
    let param = this.isProductNameAsc ? "productName=asc" : "productName=desc";
    this.getSalesReport(this.yearMonth, param);
  }

  sortQuantity() {
    this.isQuantityAsc = !this.isQuantityAsc;
    let param = this.isQuantityAsc ? "quantity=asc" : "quantity=desc";
    this.getSalesReport(this.yearMonth, param);
  }

  sortWeight() {
    this.isWeightAsc = !this.isWeightAsc;
    let param = this.isWeightAsc ? "weight=asc" : "weight=desc";
    this.getSalesReport(this.yearMonth, param);
  }

  sortPrice() {
    this.isPriceAsc = !this.isPriceAsc;
    let param = this.isPriceAsc ? "price=asc" : "price=desc";
    this.getSalesReport(this.yearMonth, param);
  }

  getSalesReportOnChange(selected) {
    this.isCategoryAsc = false;
    this.yearMonth = selected;
    this.sortCategory();
  }
}
