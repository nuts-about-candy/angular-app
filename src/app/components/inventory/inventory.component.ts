import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryService } from 'src/app/services/inventory.service';
import { Category } from 'src/app/models/category';
import { Product } from 'src/app/models/product';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ProductClass } from 'src/app/models/productClass';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  $ : any;
  @ViewChild('productModal') productModal : any;
  @ViewChild('categoryModal') categoryModal : any;
  categories : Category[];
  selecTedCategory = {} as Category;
  productClass : ProductClass[];
  products : Product[];
  product = {} as Product;
  formGroupCategory: FormGroup;
  formGroupProduct : FormGroup;
  productModalTitle = 'Add new Product in ' + this.selecTedCategory.name;

  constructor(
    private inventoryService : InventoryService,
    private title : Title,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Inventory | Nuts About Candy")
    this.getCategories(this.selecTedCategory.id);
    this.getProductClass();
    this.initProductFormGroup(this.product);
    this.initCategoryFormGroup();
  }

  initCategoryFormGroup() {
    this.formGroupCategory = new FormGroup({
      name : new FormControl('', [Validators.required])
    })
  }
  initProductFormGroup(data : any) {
    this.formGroupProduct = new FormGroup({
      id : new FormControl(data.id),
      name : new FormControl(data.name, [Validators.required]),
      description : new FormControl(data.description),
      classificationId : new FormControl('', [Validators.required])
    })
    this.formGroupProduct.controls['classificationId']
      .setValue(data.classificationId, {onlySelf: true});
  }

  getProductClass() {
    this.inventoryService.getProductClass()
      .subscribe(
        (res : any) => this.productClass = res,
        error => console.log("Error getting product classifications."))
  }
  getCategories(categoryId) {
    this.inventoryService.getCategories()
      .subscribe((result : any) => {
        this.categories = result;
        let category = this.categories[0];

        if(categoryId > 0) {
          category = this.categories.filter(function(cat) {
            return cat.id == categoryId;
          })[0];
        }
        this.getProducts(category);
      },
      error => console.log("Error getting categories."));
  }
  getProducts(category) {
    this.selecTedCategory = category;
    this.inventoryService.getProductsByCategoryId(category.id)
      .subscribe(
        (result : any) => this.products = result,
        error => console.log("Error getting products."));
  }

  addProduct() {
    if(this.formGroupProduct.valid) {
      let product = this.formGroupProduct.value;
      product.categoryId = this.selecTedCategory.id;

      if(product.id === undefined || product.id === null) {
        this.inventoryService.addProduct(product)
          .subscribe((res : any) => {
            this.closeModal(this.productModal);
            this.initProductFormGroup(this.product);
            this.getProducts(this.selecTedCategory);
          },
          error => console.log("Error adding product."));
      }
      else {
        this.inventoryService.editProduct(product)
          .subscribe((res : any) => {
            this.closeModal(this.productModal);
            this.initProductFormGroup(this.product);
            this.getProducts(this.selecTedCategory);
          },
          error => console.log("Error in editing product."))
      }
    }
  }

  editProduct(id) {
    this.inventoryService.getProductByProductId(id)
      .subscribe((res : any) => {
        this.openModal(this.productModal, 'edit');
        this.product = res;
        this.initProductFormGroup(this.product);
        this.productModalTitle = "Edit product '" + this.product.name + "'"
      },
      error => console.log("Error in getting product details."));
  }

  deleteProduct(id, name) {
    let confirm = window.confirm("\nAre you sure you want to delete product '" + name + "'?");
    if(confirm)
      this.inventoryService.deleteProduct(id)
        .subscribe(
          res=> this.getProducts(this.selecTedCategory),
          error => console.log("Error deleting product."));

  }

  addCategory(){
    if(this.formGroupCategory.valid)
      this.inventoryService.addCategory(this.formGroupCategory.value)
        .subscribe((res : any) => {
          this.initCategoryFormGroup();
          this.closeModal(this.categoryModal);
          this.getCategories((res.id));
        },
        error => console.log("Error adding category."));
  }

  deleteCategory() {
    let confirm = window.confirm("Deleting this category will also delete all the products under this category." +
      "\nAre you sure you want to delete category '" + this.selecTedCategory.name + "'?");
    if(confirm)
      this.inventoryService.deleteCategory(this.selecTedCategory.id)
        .subscribe(
          res=> this.getCategories(0),
          error => console.log("Error deleting category."));
  }

  resetStocks() {
    let confirm = window.confirm("\nAre you sure you want to reset Stocks'?");
    if(confirm)
      this.inventoryService.resetStocks()
        .subscribe(
          res => this.getCategories(this.selecTedCategory.id),
          error => console.log("Error Resetting stocks."));
  }

  addStocks() {
    let confirm = window.confirm("\nAre you sure you want to add Stock'?");
    if(confirm)
      this.inventoryService.addStocks()
        .subscribe(
          res => this.getCategories(this.selecTedCategory.id),
          error => console.log("Error Resetting stocks."));
  }

  // MODALS
  closeModal(content) {
    this.modalService.dismissAll(content);
  }
  openModal(content, action) {
    if(action === 'add')
      this.productModalTitle = 'Add new Product in ' + this.selecTedCategory.name;

    this.modalService.open(content, {})
      .result.then(
        result => {},
        reason => this.initProductFormGroup({}));
  }

}
