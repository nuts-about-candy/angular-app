import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Product } from 'src/app/models/product';
import { Title } from '@angular/platform-browser';
import { CartItem } from 'src/app/models/cartItem';
import { User } from 'src/app/models/user';
import { Category } from 'src/app/models/category';
import { InventoryService } from 'src/app/services/inventory.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { ProductSize } from 'src/app/models/productSize';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private cartService : CartService, 
    private title : Title,
    private inventoryService : InventoryService,
    private formBuilder : FormBuilder,
    private productService : ProductService) { }

  product = {} as Product;
  cartItemCount : number = 0;
  user = {} as User;
  categories : Category[];
  selecTedCategory = {} as Category;
  products : Product[];
  formGroupProduct : FormGroup;

  ngOnInit(): void {
    this.title.setTitle("Items for Sale | Nuts About Candy")
    this.user = JSON.parse(localStorage.getItem("userDetails"));
    this.getCartItemsCount();
    this.getCategories(this.selecTedCategory);
    this.initProductFormGroup();
  }

  initProductFormGroup() {

    this.formGroupProduct = this.formBuilder.group({
      size : new FormControl('', [Validators.required]),
      quantity : new FormControl(1, [Validators.required])
    })
  }
  getCategories(categoryId) {
    this.inventoryService.getCategories()
      .subscribe((result : any) => {
        this.categories = result;
        this.getProducts(this.categories[0]);
      },
      error => console.log("Error getting categories."));
  }

  getCartItemsCount() {
    this.cartService.getCartItems(this.user.id)
      .subscribe((res : any) => {
        let checkOutDetails = res;
        this.cartItemCount = checkOutDetails.itemCount;
      },
      error => console.log("Error in getting user cart items."));
  }

  getProducts(category : Category) {
    this.selecTedCategory = category;
    this.productService.getProductsForSaleByCategoryId(category.id)
      .subscribe((result : any) => {
        this.products = result;
        this.products.map( prod => {
          prod.img = "assets/img/nac-prod/nac"+ Math.round((Math.random() * 12) + 1) + ".jpg";
        })
      },
      error => console.log("Error getting products."));
  }


  addToCart(productId : number, productSizes : ProductSize[]) {
    let cartItem = {} as CartItem;
    cartItem = this.formGroupProduct.value;
    cartItem.userId = this.user.id;
    
    if(cartItem.size === "")
      alert("Error! Please select Product size.");
    else 
      if(cartItem.quantity === null || cartItem.quantity <= 0) 
        alert("Error! Invalid value for quantity.");
      else
        this.productService.getProductForSaleByProductId(productId)
          .subscribe((res: any) => {
            let product : Product = res;
            cartItem.productDetails = product;
            cartItem.productId = product.id;

            let productSize = productSizes.filter(size => {
              return size.size == cartItem.size;
            })[0];
            cartItem.productSize = productSize;

            if((cartItem.quantity * productSize.weight) > product.stock)
              alert("Error! Selected quantity exceeds current stock.");
            else
              this.cartService.addItem(cartItem)
                .subscribe((res : any) => {
                  alert("Success! Item added to your cart.");
                  this.getCartItemsCount();
                }, error => console.log("Error adding item to cart."));

          }, error => console.log("Error getting details of Product for sale."));
  }
}
