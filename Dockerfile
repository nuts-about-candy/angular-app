FROM node:12.18-alpine as build-step
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build --prod

FROM nginx:1.18.0-alpine as prod-state
COPY --from=build-step /app/dist/nuts-about-candy /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d
EXPOSE 80
ENTRYPOINT nginx -g 'daemon off;'

# BUILD docker build -t nuts-about-candy .
# RUN docker run -p 80:80 nuts-about-candy
